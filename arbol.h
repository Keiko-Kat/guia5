#include <iostream>
#include <string>
#include <fstream>
#include "Nodo.h"
using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

class Arbol{
	private:
		Nodo *raiz = NULL;	
	public:
		Arbol();
        Nodo *get_raiz();		
		void crear (string info);
        Nodo *crear_nodo(string info);
        void open(int opt, Nodo *&raiz, int *BO);
		void InsercionBalanceado(Nodo *&raiz, int *BO, string infor);
		void Busqueda(Nodo *nodo, string infor);
		void Restructura1(Nodo *&raiz, int *BO);
		void Restructura2(Nodo *&raiz, int *BO);
		void ModificaBalanceado(Nodo *&raiz, int *BO, string infor);
		void Borra(Nodo *&aux1, Nodo *&otro1, int *BO);
		void EliminacionBalanceado(Nodo *&raiz, int *BO, string infor);
		string catch_string(string in);
		int Menu();
        int Menu_2();
		void GenerarGrafo(Nodo *p);
		int PreOrden(Nodo *, ofstream &fp, int cont);
};

#endif

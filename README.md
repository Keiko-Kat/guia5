# Guia5

EL ejercicio fue resuelto en Geany, ejecutado desde la terminal para testeo en linux 4.9.0-12-amd64


Ejecutar make

Descomprimir carpeta de datos a utilizar

Ejecutar la orden ./arbol_1

Ingresar por lo menos un dato previo a intentar abrir uno de los archivos externos

Seguir las instrucciones del menu para funcionar

ingresar 0 para salir


# Problemas

El mayor problema fue crear el grafo con los archivos entregados, por lo que decidi
dividir los archivos en lotes de 500 lineas con la orden "split -l 500 archivo" 
en la terminal para que el grafo abriera

como punto a tomar en cuenta, el grafo si se tarda unos segundos en abrir luego de 
abrir un archivo, y no pude probar abrir mas de un archivo a la vez porque el computador
no soportó el peso del archivo generado y "colapsó" mas de una vez al intentarlo

tambien entrego los archivos divididos en 7 lotes, debido a que la cantidad de archivos generados
mediante el split, hacia muy dificil el hacer que se pudiera probar toda la infromacion 
de los txt en un solo programa, por lo que, por la misma razon se sugiere que cada lote se abra
de a uno (como addendum necesito añadir que el septimo lote contiene 8 archivos menos que el resto,
debido a que el original era mas corto, por lo que existen 8 opciones que no se pueden utilizar al
ejecutar el programa con el septimo lote [las opciones de archivo de la 43 a la 50])

Favor utilizar el paquete zip, 7-c.zip, debido a que los otros archivos de nombres similares son
inaccesibles.

Addendum, favor abstenerse de intentar eliminar y/o modificar un nodo con mas de un hijo, debido a que por un error que no logre descifrar, al intentar la eliminacion se genera un error de segmento al intentar leer el arbol para el grafo, y al hacer la modificacion, se eliminan los nodos hijos y falla la reestructuracion.
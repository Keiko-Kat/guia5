#include <iostream>
#include <string>
#include <fstream>
using namespace std;

#include "Nodo.h"
#include "arbol.h"

int main(){
	int opcion, men;
	string in;
	string elemento;
	Arbol *a = new Arbol();
	Nodo *raiz = new Nodo();
	raiz = a->get_raiz();
  
	system("clear");
	opcion = a->Menu();
	int inicio = 1;
  
	while (opcion != 0) {    
		switch(opcion) {
			case 1:
				cout<<"Ingresar elemento: "<<endl;
				getline(cin, in);
				elemento = in;
				cout<<in<<endl;
				inicio = 0;
				if(raiz == NULL){
					a->crear(elemento);
					raiz = a->get_raiz();
				}else{
					a->InsercionBalanceado(raiz, &inicio, elemento);
				}
				a->GenerarGrafo(raiz);
				break;

			case 2:
				cout<<"Buscar elemento: "<<endl;
				getline(cin, in);
				elemento = in;
				a->Busqueda(raiz, elemento);
				break;
      
			case 3:
				cout<<"Eliminar elemento: "<<endl;
				getline(cin, in);
				elemento = in;
				inicio = 0;
				a->EliminacionBalanceado(raiz, &inicio, elemento);
				a->GenerarGrafo(raiz);
				break;
		
			case 4:
				a->GenerarGrafo(raiz);
				break;
			
			case 5:
				cout<<"Modificar elemento:"<<endl;
				getline(cin, in);
				elemento = in;
				a->ModificaBalanceado(raiz, &inicio, elemento);
				a->GenerarGrafo(raiz);
				break;
			case 6:
				cout<<"Escoja el archivo que abrir: "<<endl;
				men = a->Menu_2();
				a->open(men, raiz, &inicio);
				//~ raiz = a->get_raiz();
				break;
			case 0: 
				break;
		}
		opcion = a->Menu();
	}
  
	return 0;
}

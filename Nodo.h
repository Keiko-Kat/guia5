#include <iostream>
#include <string>
#include <fstream>
#ifndef NODO_H
#define NODO_H

/* define la estructura del nodo. */
typedef struct _Nodo {
    int FE;
    std::string info;
    struct _Nodo *izq = NULL;
    struct _Nodo *der = NULL;
} Nodo;

#endif

#include <iostream>
#include <string>
#include <fstream>
using namespace std;

#include "Nodo.h"
#include "arbol.h"

Arbol::Arbol() {}

//catch_string se asegura de evitar un error de conversion//
//Asegurando que solo existan numeros en el string ingresado//
string Arbol::catch_string(string in){
	//Esta funcion recibe un string ingresado por teclado en la funcion principal//
	//para validar la entrada de un int primero se crea y define is_true como verdadero por default//
	bool is_true = true;
	//Se abre un ciclo while que corre mientras is_true sea verdadero//
	while (is_true == true){
		//Dentro del while se abre un ciclo for que analiza el string recibido//
		for(int j = 0; j < in.size(); j++){
			//Se recorre y se comparan los valores ASCII contenidos en el string//
			//A los valores ASCII de los numros del 0-9 y al valor de '-'//
			if((in[j] >= 48 && in[j] <= 57) || in[j] == 45){
				//si el contenido del string coincide con un valor ASCII numerico o de "-" is_true se iguala a falso//
				is_true = false;
			}else{
				//Si el contenido del string si coincide, is_true se iguala a verdadero y se rompre el ciclo//
				is_true = true;
				break;
			}
		}
		//Luego si se encuentra algo que no es un numero o '-' en el string ingresado//
		if(is_true == true){
			//Se entrega un mensaje de error//
			cout<<"Por favor ingrese valores numéricos:"<<endl;
			//Y se pide otro ingreso//
			getline(cin, in);
		}
	}
	//De esta forma se asegura que la funcion 'stoi()' no entregue error//
	return in;
}

int Arbol::Menu() {
	int opt;
	string in;
	cout<<"--------------------"<<endl;
	cout<<"1) Insertar"<<endl;
	cout<<"2) Buscar"<<endl;
	cout<<"3) Eliminacion"<<endl;
	cout<<"4) Grafo"<<endl;
	cout<<"5) Modificar"<<endl;
	cout<<"6) Abrir archivo PDB"<<endl;
	cout<<"0) Salir"<<endl;
	cout<<"Opción: ";
	getline(cin, in);
	in = catch_string(in);
	opt = stoi(in);
	while(opt < 0 || opt >= 7){
		if(opt < 0 || opt >= 7){
			cout<<"opcion no valida"<<endl;
		}
		getline(cin, in);
		in = catch_string(in);
		opt = stoi(in);
	}
	return opt;
}

int Arbol::Menu_2(){
	int opt;
	string in;
	
	cout<<"--------------------"<<endl;
	cout<<"1) Archivo 1"<<endl;
	cout<<"2) Archivo 2"<<endl;
	cout<<"3) Archivo 3"<<endl;
	cout<<"4) Archivo 4"<<endl;
	cout<<"5) Archivo 5"<<endl;
	cout<<"6) Archivo 6"<<endl;
	cout<<"7) Archivo 7"<<endl;
	cout<<"8) Archivo 8"<<endl;
	cout<<"9) Archivo 9"<<endl;
	cout<<"10) Archivo 10"<<endl;
	cout<<"11) Archivo 11"<<endl;
	cout<<"12) Archivo 12"<<endl;
	cout<<"13) Archivo 13"<<endl;
	cout<<"14) Archivo 14"<<endl;
	cout<<"15) Archivo 15"<<endl;
	cout<<"16) Archivo 16"<<endl;
	cout<<"17) Archivo 17"<<endl;
	cout<<"18) Archivo 18"<<endl;
	cout<<"19) Archivo 19"<<endl;
	cout<<"20) Archivo 20"<<endl;
	cout<<"21) Archivo 21"<<endl;
	cout<<"22) Archivo 22"<<endl;
	cout<<"23) Archivo 23"<<endl;
	cout<<"24) Archivo 24"<<endl;
	cout<<"25) Archivo 25"<<endl;
	cout<<"26) Archivo 26"<<endl;
	cout<<"27) Archivo 27"<<endl;
	cout<<"28) Archivo 28"<<endl;
	cout<<"29) Archivo 29"<<endl;
	cout<<"30) Archivo 30"<<endl;
	cout<<"31) Archivo 31"<<endl;
	cout<<"32) Archivo 32"<<endl;
	cout<<"33) Archivo 33"<<endl;
	cout<<"34) Archivo 34"<<endl;
	cout<<"35) Archivo 35"<<endl;
	cout<<"36) Archivo 36"<<endl;
	cout<<"37) Archivo 37"<<endl;
	cout<<"38) Archivo 38"<<endl;
	cout<<"39) Archivo 39"<<endl;
	cout<<"40) Archivo 40"<<endl;
	cout<<"41) Archivo 41"<<endl;
	cout<<"42) Archivo 42"<<endl;
	cout<<"43) Archivo 43"<<endl;
	cout<<"44) Archivo 44"<<endl;
	cout<<"45) Archivo 45"<<endl;
	cout<<"46) Archivo 46"<<endl;
	cout<<"47) Archivo 47"<<endl;
	cout<<"48) Archivo 48"<<endl;
	cout<<"49) Archivo 49"<<endl;
	cout<<"50) Archivo 50"<<endl;
	cout<<"0) Salir"<<endl;
	cout<<"Opción: ";	
	getline(cin, in);
	in = catch_string(in);
	opt = stoi(in);
	while(opt < 0 || opt > 51){
		if(opt < 0 || opt > 51){
			cout<<"opcion no valida"<<endl;
		}
		getline(cin, in);
		in = catch_string(in);
		opt = stoi(in);
	}
	return opt;
}


void Arbol::Busqueda(Nodo *nodo, string infor) {
	if (nodo != NULL) {
		if (infor < nodo->info) {
			Busqueda(nodo->izq, infor);
		} else {
			if (infor > nodo->info) {
				Busqueda(nodo->der,infor);
			} else {
				cout<<"El nodo SI se encuentra en el árbol\n";
			}
		}
	} else {
		cout<<"El nodo NO se encuentra en el árbol\n";
	}
}

Nodo *Arbol::crear_nodo(string info){
	Nodo *tmp;
	/*se crea el nodo*/
	tmp = new Nodo;
	
	//se inicializan las variables internas
	//con los valores base o el ingresado por el usuario
	
	tmp->info = info;
	tmp->FE = 0;
	tmp->izq = NULL;
	tmp->der = NULL;
	//se retorna el nodo
	return tmp;
}

Nodo *Arbol::get_raiz(){
	//retorna la raiz del arbol
	return this->raiz;
}

void Arbol::InsercionBalanceado(Nodo *&raiz, int *BO, string infor) {
	Nodo *nodo1 = NULL;
	Nodo *nodo2 = NULL; 
	Nodo *temp = crear_nodo(infor);
	
  
	if(raiz != NULL) {
		if (infor < raiz->info) {
			InsercionBalanceado(raiz->izq, BO, infor);
			if(raiz->izq == NULL){
				//si no existen mas nodos hacia la izquierda, el nodo nuevo se inserta a la izquierda de raiz
				raiz->izq = temp;
				*BO = 1;
			}
			if(*BO == 1) {
				switch (raiz->FE) {
					case 1: 
						raiz->FE = 0;
						*BO = 0;
						break;
        
					case 0: 
						raiz->FE = -1;
						break;
     
					case -1: 
						/* reestructuración del árbol */
						nodo1 = raiz->izq;
            
						/* Rotacion II */
						if (nodo1->FE <= 0) { 
							raiz->izq = nodo1->der;
							nodo1->der = raiz;
							raiz->FE = 0;
							raiz = nodo1;
            
						} else { 
							/* Rotacion ID */
							nodo2 = nodo1->der;
							raiz->izq = nodo2->der;
							nodo2->der = raiz;
							nodo1->der = nodo2->izq;
							nodo2->izq = nodo1;
         
							if (nodo2->FE == -1){
								raiz->FE = 1;
							}else{
								raiz->FE = 0;
							}
							if (nodo2->FE == 1){
								nodo1->FE = -1;
							}else{
								nodo1->FE = 0;
							}
							raiz = nodo2;
						}
            
						raiz->FE = 0;
						*BO = 0;
						break;
				}
			} 
      
		} else {
      
			if (infor > raiz->info) {
				InsercionBalanceado(raiz->der, BO, infor);
				if(raiz->der == NULL){
					//si ya no hay mas nodos hacia la derecha, el nuevo se ingresa a la derecha de raiz
					raiz->der = temp;
					*BO = 1;
				}
				if (*BO == 1) {          
					switch (raiz->FE) {
						case -1: 
							raiz->FE = 0;
							*BO = 0;
							break;
						
						case 0: 
							raiz->FE = 1;
							break;
				  
						case 1: 
							/* reestructuración del árbol */
							nodo1 = raiz->der;
						  
							if (nodo1->FE >= 0) { 
								/* Rotacion DD */
								raiz->der = nodo1->izq;
								nodo1->izq = raiz;
								raiz->FE = 0;
								raiz = nodo1;
						
							} else { 
								/* Rotacion DI */
								nodo2 = nodo1->izq;
								raiz->der = nodo2->izq;
								nodo2->izq = raiz;
								nodo1->izq = nodo2->der;
								nodo2->der = nodo1;
								
								if (nodo2->FE == 1){
									raiz->FE = -1;
								}else{
									raiz->FE = 0;
								}
								if (nodo2->FE == -1){
									nodo1->FE = 1;
								}else{
									nodo1->FE = 0;
								}
								raiz = nodo2;
							}
				  
							raiz->FE = 0;
							*BO = 0;
							break;
					}		
				}
			} else {
				cout<<"El nodo ya se encuentra en el árbol\n";
			}
		}
	}
}
void Arbol::crear(string info){
	//esta funcion crea un nodo y lo ubica como raiz
	Nodo *tmp;
	//se crea el nodo
	tmp = new Nodo;
	
	//se inicializan las variables internas
	//con los valores base o el ingresado por el usuario
	tmp->info = info;
	tmp->FE = 0;
	tmp->izq = NULL;
	tmp->der = NULL;
	//se ubica el nodo en posicion raiz
	this->raiz = tmp;
}


void Arbol::Restructura1(Nodo *&raiz, int *BO) {
	Nodo *nodo1, *nodo2; 
	//~ nodo = *raiz;
  
	if (*BO == 1) {
		switch (raiz->FE) {
			case -1: 
				raiz->FE = 0;
				break;
  
			case 0: 
				raiz->FE = 1;
				*BO = 0;
				break;

			case 1: 
				/* reestructuracion del árbol */
				nodo1 = raiz->der;
  
				if (nodo1->FE >= 0) { 
					/* rotacion DD */	
					raiz->der = nodo1->izq;
					nodo1->izq = raiz;
					
					switch (nodo1->FE) {
						case 0: 
							raiz->FE = 1;
							nodo1->FE = -1;
							*BO = 0;
							break;
						case 1: 	
							raiz->FE = 0;
							nodo1->FE = 0;
							*BO = 0;
							break;           
					}
					raiz = nodo1;
				} else { 
					/* Rotacion DI */
					nodo2 = nodo1->izq;
					raiz->der = nodo2->izq;
					nodo2->izq = raiz;
					nodo1->izq = nodo2->der;
					nodo2->der = nodo1;
   
					if (nodo2->FE == 1){
						raiz->FE = -1;
					}else{
						raiz->FE = 0;
					}
					if (nodo2->FE == -1){
						nodo1->FE = 1;
					}else{
						nodo1->FE = 0;
					}
					raiz = nodo2;
					nodo2->FE = 0;       
				} 
				break;   
		}
	}
}


void Arbol::Restructura2(Nodo *&raiz, int *BO) {
	Nodo *nodo, *nodo1, *nodo2; 
	//~ nodo = *raiz;
	
	if (*BO == 1) {
		switch (raiz->FE) {
			case 1: 
				raiz->FE = 0;
				break;
			case 0: 
				raiz->FE = -1;
				*BO = 0;
				break;
			case -1: 
				/* reestructuracion del árbol */
				nodo1 = raiz->izq;
				if (nodo1->FE <= 0) { 
					/* rotacion II */
					raiz->izq = nodo1->der;
					nodo1->der = raiz;
					switch (nodo1->FE) {
							case 0: 
								raiz->FE = -1;
								nodo1->FE = 1;
								*BO = 0;
								break;
							case -1: 
								raiz->FE = 0;
								nodo1->FE = 0;
								//~ *BO = 0;
								break;
					}
					raiz = nodo1;
				} else { 
					/* Rotacion ID */
					nodo2 = nodo1->der;
					raiz->izq = nodo2->der;
					nodo2->der = raiz;
					nodo1->der = nodo2->izq;
					nodo2->izq = nodo1;
		
					if (nodo2->FE == -1){
						raiz->FE = 1;
					}else{
						raiz->FE = 0;
					}
					if (nodo2->FE == 1){
						nodo1->FE = -1;
					}else{
						nodo1->FE = 0;
					}
					raiz = nodo2;
					nodo2->FE = 0;       
				}      
				break;   
		}
	}
}

void Arbol::Borra(Nodo *&aux1, Nodo *&otro1, int *BO) {
	if (aux1->der != NULL) {
		Borra(aux1->der, otro1, BO); 
		Restructura2(aux1, BO);
	} else {
		otro1->info = aux1->info;
		aux1 = aux1->izq;
		*BO = 1;
	}
}

void Arbol::EliminacionBalanceado(Nodo *&raiz, int *BO, string infor) {
	Nodo *nodo, *otro; 
  
	if (raiz != NULL) {
		if (infor < raiz->info) {
			EliminacionBalanceado(raiz->izq, BO ,infor);
			Restructura1(raiz, BO);
		} else {
			if (infor > raiz->info) {	
				EliminacionBalanceado(raiz->der, BO, infor);
				Restructura2(raiz, BO); 
			} else {
				otro = raiz;
				if (otro->der == NULL) {
					raiz = otro->izq; 
					*BO = 1;
				} else {
					if (otro->izq == NULL) {
						raiz = otro->der;    
					} else {
						*BO = 1;
						Borra(otro->izq, otro, BO);
						Restructura1(raiz, BO);
						delete otro;
						otro = nullptr;
					}
				}
			}
		} 
	} else {
		cout<<"El nodo NO se encuentra en el árbol\n";
	}
}

void Arbol::ModificaBalanceado(Nodo *&raiz, int *BO, string infor){
	Nodo *nodo, *otro; 
	string in;
  
	if (raiz != NULL) {
		if (infor < raiz->info) {
			EliminacionBalanceado(raiz->izq, BO ,infor);
			Restructura1(raiz, BO);
		} else {
			if (infor > raiz->info) {	
				EliminacionBalanceado(raiz->der, BO, infor);
				Restructura2(raiz, BO); 
			} else {
				otro = raiz;
				if (otro->der == NULL) {
					raiz = otro->izq; 
					*BO = 1;
				} else {
					if (otro->izq == NULL) {
						raiz = otro->der;    
					} else {
						*BO = 1;
						Borra(otro->izq, otro, BO);
						Restructura1(raiz, BO);
						delete otro;
						otro = nullptr;
					}
				}
			}
		} 
		cout<<"Ingresar elemento a insertar en su lugar: "<<endl;
		getline(cin, in);
		infor = in;
		InsercionBalanceado(raiz, BO, infor); 
	} else {
		cout<<"El nodo NO se encuentra en el árbol\n";
	}
}

void Arbol::GenerarGrafo(Nodo *ArbolInt) {
	int cont =0;
	ofstream fp;
  
	fp.open("grafo.txt");
	fp << "digraph G {"<<endl;
	fp << "node [style=filled fillcolor=magenta];" <<endl;
	
	fp << "null [shape=point];\n"; 
	fp <<"null->"<< '"'<<ArbolInt->info<< '"'<<" [label=" <<ArbolInt->FE<< "];"<< endl; 
	cont = PreOrden(ArbolInt, fp, cont);
  
	fp << "}"<<endl;
	fp.close();
  
	system("dot -Tpng -ografo.png grafo.txt");
	system("eog grafo.png &");
	
	
}

int Arbol::PreOrden(Nodo *p, ofstream &archivo, int cont) {
		string infoTmp;
  /* Se enlazan los nodos del grafo, para diferencia entre izq y der a cada nodo se le entrega un identificador al final, siendo i: izquierda
   * y d: derecha, esto se cumplirá para los casos en donde los nodos no apunten a ningún otro (nodos finales) 
   * */
	if (p != NULL) {
		infoTmp = p->info; 
		archivo << '"'<< infoTmp <<'"'<<  " [style=filled fillcolor=magenta];" << endl;
		/* Por cada nodo ya sea por izq o der se escribe dentro de la instancia del archivo */  
		if (p->izq != NULL) {
			archivo<<'"'<< p->info<<'"' << "->" << '"'<< p->izq->info <<'"'<<  " [label="<< p->izq->FE<<"];" << endl;
			archivo << '"'<<p->izq->info<<'"' << " [style=filled fillcolor=magenta];" << endl;
      
		} else {
			infoTmp = p->info;
			archivo <<"null" << cont << " [shape=point]"  << endl;
			archivo <<'"'<< p->info<<'"' <<"->" << "null" << cont<< endl;
			cont = cont+1;
		}
    
		if (p->der != NULL) {
			archivo <<'"'<<  p->info << '"'<< "->" << '"'<< p->der->info << '"'<< " [label="<< p->der->FE<<"];" << endl;
			archivo << '"'<< p->der->info <<'"'<<  " [style=filled fillcolor=magenta];" << endl;
		
		} else {
			infoTmp = p->info;
			archivo <<"null" << cont << " [shape=point]"  << endl;
			archivo <<'"'<< p->info<<'"' << "->" << "null" << cont   << endl;
			cont = cont+1;
		}
   
		/* Se realizan los llamados tanto por la izquierda como por la derecha para la creación del grafo */
		cont = PreOrden(p->izq, archivo, cont);
		cont = PreOrden(p->der, archivo, cont); 

	}
	return cont;
}


void Arbol::open (int opt, Nodo *&raiz, int *BO){
	ifstream file("xaa");
	ifstream file2("xab");
	ifstream file3("xac");
	ifstream file4("xad");
	ifstream file5("xae");
	ifstream file6("xaf");
	ifstream file7("xag");
	ifstream file8("xah");
	ifstream file9("xai");
	ifstream file10("xaj");
	ifstream file11("xak");
	ifstream file12("xal");
	ifstream file13("xam");
	ifstream file14("xan");
	ifstream file15("xao");
	ifstream file16("xap");
	ifstream file17("xaq");
	ifstream file18("xar");
	ifstream file19("xas");
	ifstream file20("xat");
	ifstream file21("xau");
	ifstream file22("xav");
	ifstream file23("xaw");
	ifstream file24("xax");
	ifstream file25("xay");
	ifstream file26("xaz");
	ifstream file27("xba");
	ifstream file28("xbb");
	ifstream file29("xbc");
	ifstream file30("xbd");
	ifstream file31("xbe");
	ifstream file32("xbf");
	ifstream file33("xbg");
	ifstream file34("xbh");
	ifstream file35("xbi");
	ifstream file36("xbj");
	ifstream file37("xbk");
	ifstream file38("xbl");
	ifstream file39("xbm");
	ifstream file40("xbn");
	ifstream file41("xbo");
	ifstream file42("xbp");
	ifstream file43("xbq");
	ifstream file44("xbr");
	ifstream file45("xbs");
	ifstream file46("xbt");
	ifstream file47("xbu");
	ifstream file48("xbv");
	ifstream file49("xbw");
	ifstream file50("xbx");
	string line;
	switch(opt){
		case 1:			
			if(file.is_open()){
				while(getline(file, line)){
					InsercionBalanceado(raiz, BO, line);
				}				
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
			
		case 2:
			if(file2.is_open()){
				while(getline(file2, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
			
			
		case 3:
			if(file3.is_open()){
				while(getline(file3, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;	
			
		case 4:
			if(file4.is_open()){
				while(getline(file4, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
			
			
		case 5:
			if(file5.is_open()){
				while(getline(file5, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
			
			
		case 6:
			if(file6.is_open()){
				while(getline(file6, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
			
			
		case 7:
			if(file7.is_open()){
				while(getline(file7, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 8:
			if(file8.is_open()){
				while(getline(file8, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 9:
			if(file9.is_open()){
				while(getline(file9, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 10:
			if(file10.is_open()){
				while(getline(file10, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 11:
			if(file11.is_open()){
				while(getline(file11, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 12:
			if(file12.is_open()){
				while(getline(file12, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 13:
			if(file13.is_open()){
				while(getline(file13, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 14:
			if(file14.is_open()){
				while(getline(file14, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 15:
			if(file15.is_open()){
				while(getline(file15, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 16:
			if(file16.is_open()){
				while(getline(file16, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 17:
			if(file17.is_open()){
				while(getline(file17, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 18:
			if(file18.is_open()){
				while(getline(file18, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 19:
			if(file19.is_open()){
				while(getline(file19, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 20:
			if(file20.is_open()){
				while(getline(file20, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 21:
			if(file21.is_open()){
				while(getline(file21, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 22:
			if(file22.is_open()){
				while(getline(file22, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 23:
			if(file23.is_open()){
				while(getline(file23, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 24:
			if(file24.is_open()){
				while(getline(file24, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 25:
			if(file25.is_open()){
				while(getline(file25, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 26:
			if(file26.is_open()){
				while(getline(file26, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 27:
			if(file27.is_open()){
				while(getline(file27, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 28:
			if(file28.is_open()){
				while(getline(file28, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 29:
			if(file29.is_open()){
				while(getline(file29, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 30:
			if(file30.is_open()){
				while(getline(file30, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 31:
			if(file31.is_open()){
				while(getline(file31, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 32:
			if(file32.is_open()){
				while(getline(file32, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 33:
			if(file33.is_open()){
				while(getline(file33, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 34:
			if(file34.is_open()){
				while(getline(file34, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 35:
			if(file35.is_open()){
				while(getline(file35, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 36:
			if(file36.is_open()){
				while(getline(file36, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 37:
			if(file37.is_open()){
				while(getline(file37, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 38:
			if(file38.is_open()){
				while(getline(file38, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 39:
			if(file39.is_open()){
				while(getline(file39, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 40:
			if(file40.is_open()){
				while(getline(file40, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 41:
			if(file41.is_open()){
				while(getline(file41, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 42:
			if(file42.is_open()){
				while(getline(file42, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 43:
			if(file43.is_open()){
				while(getline(file43, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 44:
			if(file44.is_open()){
				while(getline(file44, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 45:
			if(file45.is_open()){
				while(getline(file45, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 46:
			if(file46.is_open()){
				while(getline(file46, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 47:
			if(file47.is_open()){
				while(getline(file47, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 48:
			if(file48.is_open()){
				while(getline(file48, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 49:
			if(file49.is_open()){
				while(getline(file49, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
		case 50:
			if(file50.is_open()){
				while(getline(file50, line)){
					InsercionBalanceado(raiz, BO, line);
				}
				GenerarGrafo(raiz);
			}else{
				cout<<"El archivo no se puede abrir"<<endl;
			}
			break;				
			
			
		default:
			cout<<"Opcion incorrecta"<<endl;
			break;
		}
}
